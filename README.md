COMP1005 2023 Tutorial 6
========================


## Background

* [File I/O](https://www.tutorialspoint.com/cprogramming/c_file_io.htm)
* [Structs](https://www.tutorialspoint.com/cprogramming/c_structures.htm)
* [Structs and Typedef](https://www.learn-c.org/en/Structures)
    * [Typedef](https://www.tutorialspoint.com/cprogramming/c_typedef.htm)

## Exercises


0. Write a program in C to create and store information in a text file.  
    ***Expected Output:*** 
    ```
    Input a sentence for the file : This is the content of the file test.txt. 
    
    The file test.txt created successfully...!!  
    ```

0. Write a program in C to read an existing file.  
    ***Expected Output:*** 
    ```
    Input the file name to be opened: test.txt 
    
    The content of the file test.txt is:                                                                       
    This is the content of the file test.txt.  
    ```

0. Write a program in C to create a struct array of size n with dynamic memory allocation. Store randomly generated integer numbers (from 1 to n) and the cumulative sum of these numbers into the struct array with the order of indices.  
    ***Hint :***
    ```
    Use 'rand() % n + 1' to generate random numbers.
    ```
    ***Expected Output:*** 
    ```
    Please input the size (n) of array (n <= 20): 5
    
    The 5 numbers stored in the struct array are: 
    3 2 4 1 2
    The cumulative sum of these numbers are:: 
    3 5 9 10 12
    ```


## Extras


0. Write a program in C to write multiple lines in a text file.  
    ***Expected Output:*** 
    ```
    Input the number of lines to be written: 4 
    Input the lines: 
    test line 1 
    test line 2 
    test line 3 
    test line 4 

    The file test.txt created successfully...!! 
    ```

0. Write a program in C to find the content of the file and number of lines in a Text File.  
    ***Expected Output:*** 
    ```
    Input the file name to be opened: test.txt 
    
    The content of the file test.txt is:                                                                      
    test line 1                                                                                                  
    test line 2                                                                                                  
    test line 3                                                                                                  
    test line 4                                                                                                  
    The number of lines in the file is: 4
    ```

0. Write a program in C to create a 2D struct array of size (m, n) with dynamic memory allocation. Store randomly generated integer numbers (from 1 to m*n) and the cumulative sum of these numbers.  
    ***Expected Output:*** 
    ```
    Input the row number m: 2
    Input the column number n: 3
    
    The desired matrix (m=2, n=3) is:
     5  3  6
     3  2  5
    The cumulative sum (by row) of the matrix above is: 
     5  8 14
    17 19 24 
    ```

0. Try other string and I/O operations.

