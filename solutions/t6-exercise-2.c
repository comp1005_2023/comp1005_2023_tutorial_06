
#include <stdio.h>
#include <stdlib.h>
#include "t6-exercise-2.h"

int main() {
    node *array;
    int n, i, sum;

    printf("Please input the size of array (1 to 20):");
    scanf("%d", &n);
    if (n < 1 || n > 20) {
        printf("The number given is out of range.");
        return 1;
    }

    array = (node *) malloc(n * sizeof(node));
    sum = 0;
    for (i = 0; i < n; i++) {
        array[i].num = rand() % n + 1;
        sum += array[i].num;
        array[i].sum = sum;
    }

    printf("The %d numbers stored in the struct node array are:\n", n);
    for (i = 0; i < n; i++) {
        printf("%5d ", array[i].num);
    }
    printf("\n");

    printf("The cumulative sum of these numbers are:\n");
    for (i = 0; i < n; i++) {
        printf("%5d ", array[i].sum);
    }
    printf("\n");

    free(array);
    return 0;
}
