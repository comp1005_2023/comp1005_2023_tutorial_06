
#include <stdio.h>
#include <stdlib.h>
#include "t6-extra-2.h"

int main() {
    node **array;
    int m, n, i, j, sum;

    printf("Please input two numbers as the size of the matrix (maximum 10 x 10): ");
    scanf("%d %d", &m, &n);
    if (m < 1 || m > 10 || n < 1 || n > 10) {
        printf("The number(s) given is(are) out of range.");
        return 1;
    }

    printf("\nThe matrix of (m=%d, n=%d) generated is:\n", m, n);

    array = (node **) malloc(m * sizeof(node *));
    for (i = 0, sum = 0; i < m; i++) {
        array[i] = (node *) malloc(n * sizeof(node));
        for (j = 0; j < n; j++) {
            array[i][j].num = rand() % (m * n) + 1;
            printf("%5d ", array[i][j].num);
            array[i][j].sum = sum += array[i][j].num;
        }
        printf("\n");
    }

    printf("\nThe cumulative sum of elements (by row) in the above matrix is:\n");

    for (i = 0; i < m; i++) {
        for (j = 0; j < n; j++) {
            printf("%5d ", array[i][j].sum);
        }
        printf("\n");
    }

    for (i = 0; i < m; i++) {
        free(array[i]);
    }
    free(array);
    return 0;
}
